﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    class TestCasePage : PageBase
    {
        #region Mapping
        private By casoDeTesteTab = By.XPath("//*[@id='li-test-cases-link']");
        By identificadorBy = By.XPath("//td[2][1]");
        #endregion

        #region Actions
        public void ClicarEmCasosDeTeste()
        {
            WaitForElement(casoDeTesteTab);
            Click(casoDeTesteTab);
        }

        public string RetornarTextoDoIdentificador()
        {
            return driver.FindElement(identificadorBy).Text;
        }
        #endregion
    }
}
