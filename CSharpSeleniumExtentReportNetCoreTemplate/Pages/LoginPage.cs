using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class LoginPage : PageBase
    {
        #region Mapping 
            #region Mapping Login
            private By usernameField = By.Id("user_email");
            private By passwordField = By.Id("user_password");
            private By loginButton = By.XPath("//input[@type='submit']");
            #endregion

            #region Mapping Logout
            private By dropddownMenu = By.XPath("//*[@id='wrapper']/nav/ul/li/a");
            private By dropdownMenuSairOption = By.XPath("//*[@id='wrapper']/nav/ul/li/ul/li[2]/a");
            #endregion

            #region Mapping Redefinir Senha
            private By esqueceuASenhaText = By.XPath("//div/a[contains(text(), 'Esqueceu a senha')]");
            private By enviarButton = By.XPath("//button[contains(text(), 'Enviar')]");
            #endregion

            #region Mapping Cadastro
            private By cadastreSeText = By.XPath("//div/a[contains(text(), 'Cadastrar-se')]");
            private By loginField = By.Id("user_login");
            private By nomeField = By.Id("user_name");
            private By sobrenomeField = By.Id("user_lastname");
            private By userPasswordConfirmationField = By.Id("user_password_confirmation");
            private By cadastreSeButton = By.Id("cadastrar_se");
            #endregion

            #region Mapping Mensagens
            private By mensagemDeErro = By.XPath("//div[@class='alert alert-danger alert-dismissable']");
            private By mensagemConfirmaLogin = By.XPath("//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-success']");
            private By mensagemLoginObrigatorio = By.Id("elementErrorMsg_user_login");
            private By mensagemEmailObrigatorio = By.Id("elementErrorMsg_user_email");
            private By mensagemNomeObrigatorio = By.Id("elementErrorMsg_user_name");
            private By mensagemSobrenomeObrigatorio = By.Id("elementErrorMsg_user_lastname");
            private By mensagemTipoDeSenha = By.Id("elementErrorMsg_user_password");
            private By mensagemConfirmacaoDeSenha = By.Id("elementErrorMsg_user_password_confirmation");
            private By mensagemLogout = By.XPath("//*[@id='column-login']/div/div[2]/div");
            #endregion
        #endregion

        #region Actions

            #region Mensagens e Alertas
            //todo rever mapeamento
            public string RetornarMensagemDeSucesso()
            {
                return GetText(mensagemConfirmaLogin);
            }
            public string RetornarMensagemLoginObrigatorio()
            {
                return GetText(mensagemLoginObrigatorio);
            }
            public string RetornarMensagemEmailObrigatorio()
            {
                return GetText(mensagemEmailObrigatorio);
            }
            public string RetornarMensagemNomeObrigatorio()
            {
                return GetText(mensagemNomeObrigatorio);
            }
            public string RetornarMensagemSobrenomeObrigatorio()
            {
                return GetText(mensagemSobrenomeObrigatorio);
            }
            public string RetornarMensagemTipoSenha()
            {
                return GetText(mensagemTipoDeSenha);
            }
            public string RetornarMensagemConfirmacaoDeSenhaObrigatorio()
            {
                return GetText(mensagemConfirmacaoDeSenha);
            }
            #endregion

            #region A��es Login
            public void PreencherUsuario(string usuario)
            {
                SendKeys(usernameField, usuario);
            }
            public void PreencherSenha(string senha)
            {
                SendKeys(passwordField, senha);
            }
            public void ClicarEmLogin()
            {
                Click(loginButton);
            }
            #endregion

            #region A��es Redefinir Senha
            public void ClicarEmEsqueceuASenha()
            {
                Click(esqueceuASenhaText);
            }
            public void ClicarEmEnviar()
            {
                Click(enviarButton);
            }
            public string RetornarMensagemDeErro()
            {
                return GetText(mensagemDeErro);
            }
            #endregion

            #region A��es Cadastrar
            public void ClicarEmCadastrarSe()
            {
                Click(cadastreSeText);
            }
            public void PreencherLogin(string login)
            {
                SendKeys(loginField,login);
            }
            public void PreencherNome(string nome)
            {
                SendKeys(nomeField,nome);
            }
            public void PreencherSobrenome(string sobrenome)
            {
                SendKeys(sobrenomeField,sobrenome);
            }
            public void PreencherSenhaNovamente(string senha)
            {
                SendKeys(userPasswordConfirmationField,senha);
        
            }
            public void ClicarEmCadastreSe()
            {
                Click(cadastreSeButton);
            }
            #endregion

            #region A��es Logout
            public void ClicarEmDropdownMenu()
            {
                Click(dropddownMenu);
            }
            public void ClicarEmSair()
            {
                Click(dropdownMenuSairOption);
            }
            public string RetornarMensagemDeLogout()
            {
                return GetText(mensagemLogout);
            }
            #endregion

        #endregion
    }
}
