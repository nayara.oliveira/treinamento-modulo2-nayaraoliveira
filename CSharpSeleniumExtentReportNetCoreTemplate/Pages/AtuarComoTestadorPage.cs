﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    class AtuarComoTestadorPage : PageBase
    {

        #region Mapping
        By extratoMenuBy = By.XPath("//*[@id='wrapper']/nav/div[2]/div/ul/a[1]/li/div");
        
        #endregion

        public void ClicarEmExtrato()
        {
            Click(extratoMenuBy);
        }

    }
}
