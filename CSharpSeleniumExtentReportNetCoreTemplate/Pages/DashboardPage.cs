﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class DashboardPage : PageBase
    {
        #region Mapping       
        private By projectsMenu = By.XPath("//*[@id='wrapper']/nav/div[2]/div/ul/a[2]/li");
        private By testadorMenu = By.XPath("//*[@id='wrapper']/nav/div[2]/div/ul/a[3]/li/div");
        #endregion

        #region Actions
        public void AcessarProjetos()
        {
            Click(projectsMenu);
        }

        public void AcessarPaginaDeTestador()
        {
            Click(testadorMenu);
        }
        #endregion
    }
}
