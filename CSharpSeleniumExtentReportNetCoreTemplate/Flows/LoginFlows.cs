﻿using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Flows
{
    public class LoginFlows
    {
        LoginPage loginPage;

        #region Page Object and Constructor
        public LoginFlows()
        {
            loginPage = new LoginPage();
        }
        #endregion

        public void EfetuarLogin(string username, string password)
        {
            loginPage.PreencherUsuario(username);
            loginPage.PreencherSenha(password);
            loginPage.ClicarEmLogin();
        }

        public void RedefinirSenha(string username)
        {
            loginPage.ClicarEmEsqueceuASenha();
            loginPage.PreencherUsuario(username);
            loginPage.ClicarEmEnviar();
        }

        public void CadastrarUsuario(string login, string email, string nome,string sobrenome,string senha)
        {
            loginPage.ClicarEmCadastrarSe();
            loginPage.PreencherLogin(login);
            loginPage.PreencherUsuario(email);
            loginPage.PreencherNome(nome);
            loginPage.PreencherSobrenome(sobrenome);
            loginPage.PreencherSenha(senha);
            loginPage.PreencherSenhaNovamente(senha);
            loginPage.ClicarEmCadastreSe();
        }

        public void EfetuarLogout()
        {
            loginPage.ClicarEmDropdownMenu();
            loginPage.ClicarEmSair();
        }
        
    }
}
