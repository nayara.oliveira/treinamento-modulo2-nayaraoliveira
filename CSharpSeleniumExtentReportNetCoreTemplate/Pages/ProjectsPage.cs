﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    class ProjectsPage : PageBase
    {

        #region Mapping Editar Projeto
        private By primeiroProjetoTableCellText = By.XPath("//td[3][contains(text(),'1')]  ");
        private By editarProjetoButton = By.XPath("//a[@class='ppt-btn-default ppt-btn-edit']");
        private By nomeDoProjetoField = By.XPath("//input[@id='project_name']");
        private By alterarProjetoButton = By.Id("btn-save-project");
        #endregion

        #region Mapping mensagem
        private By mensagemSucesso = By.XPath("/html/body/div[2]/div/div[2]/div/span");
        #endregion

        #region Mapping Criar Projeto
        private By incluirButton = By.Id("btn-add");
        private By identificadorField = By.Id("project_identifier");
        private By descricaoField = By.Id("project_description");
        private By tipoSelect = By.Id("project_project_type_id");
        #endregion

        #region Mapping Excluir Projeto
        private By excluirButton = By.XPath("//table[@id='dataTables-projects']/tbody/tr/td[7]/a[3]");
        private By nomeProjetoText = By.XPath("//*[@id='dataTables-projects']/tbody/tr[1]/td[2]");
        #endregion

        #region Mensagens
        private By mensagemSucessoAlert = By.XPath("//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-success']");
        private By mensagemErroIdentificadorIgualAlert = By.Id("elementErrorMsg_project_identifier");
        #endregion


        #region Editar Projeto
        public void ClicarEmPrimeiroProjeto()
        { 
            Click(primeiroProjetoTableCellText);
        }

        public void ClicarEmEditarProjeto()
        {
            Click(editarProjetoButton);
        }

        public void PreencherNomeProjeto(string nome)
        {
            ClearAndSendKeys(nomeDoProjetoField, nome);
        }    
        
        public void SalvarAlteracao()
        {
            Click(alterarProjetoButton);
        }
       
        public string RetornarMensagemDeSucesso()
        {
            return GetText(mensagemSucessoAlert);
        }
        
        public bool RetornarMensagemDeErroIdentificadorIgual()
        {
            return ReturnIfElementIsEnabled(mensagemErroIdentificadorIgualAlert);         
        }

        public string RetornarNomeDoProjeto()
        {
            return GetText(nomeDoProjetoField);
        }

        #region Criar Projeto
        public void ClicarEmIncluir()
        {
            Click(incluirButton);
        }

        public void PreencherIdentificador(string identificador)
        {
            SendKeys(identificadorField,identificador);
        }

        public void PreencherDescricao(string descricao)
        {
            SendKeys(descricaoField,descricao);
        }
 
        public void SelecionarTipoDeProjeto(string index)
        {
            ComboBoxSelectByVisibleText(tipoSelect, index);
        }
        #endregion

        public string GetSuccessMessage()
        {
            return GetText(mensagemSucesso);
        }
        public string GetNomeProjeto()
        {
            return GetText(nomeProjetoText);
        }

        #region Excluir Projeto
        public void ExcluirProjetoById()
        {
            acceptNextAlert = true;
            driver.FindElement(excluirButton).Click();
            Assert.IsTrue(Regex.IsMatch(CloseAlertAndGetItsText(), "^Deseja realmente apagar este dado[\\s\\S]$"));
        }
        #endregion
        #endregion
    }
}


