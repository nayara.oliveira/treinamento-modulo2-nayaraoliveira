﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class ProjectsTests : TestBase
    {
        LoginFlows loginFlows;
        MainFlows mainFlows;
        DashboardFlows dashboardFlows;
        ProjectsPage projectsPage;
        MainPage mainPage;
        DashboardPage dashboardPage;
        ProjectsFlows projectsFlows;
        ProjectPage projectPage;
        ProjectFlows projectFlows;
        AtuarComoTestadorFlow atuarComoTestadorFlow;

        #region Parametros
        string nomeProjeto = "";
        string identificador = "";
        string descricao = "";
        string tipo = "";
        string mensagemSucesso = "";
        string primeiroCasoDeTeste = "";
        string instrucoes = "";
        string nomeRelease = "";
        string vagas = "";
        string valorMinimo = "";
        #endregion

        public void AcessarProjetosFlow()
        {
            #region Page, Flows Objects
            loginFlows = new LoginFlows();
            mainFlows = new MainFlows();
            dashboardFlows = new DashboardFlows();
            projectsPage = new ProjectsPage();
            mainPage = new MainPage();
            projectsFlows = new ProjectsFlows();
            #endregion

            string usuario = BuilderJson.ReturnParameterAppSettings("USER");
            string senha = BuilderJson.ReturnParameterAppSettings("PASSWORD");

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciarProjetos();
            dashboardFlows.AcessarPaginaDeProjetos();

        }

        [Test]
        public void EditarProjeto()
        {
            #region Page, Flows Objects
            projectsPage = new ProjectsPage();
            projectsFlows = new ProjectsFlows();
            #endregion

            #region Parametros
            nomeProjeto = "Treinamento WEB Nayara Oliveira2";
            #endregion

            AcessarProjetosFlow();
            projectsFlows.EditarDadosDeProjeto(nomeProjeto);

            mensagemSucesso = projectsPage.RetornarMensagemDeSucesso();
            Assert.AreEqual(mensagemSucesso, "O Projeto ( " + nomeProjeto + " ) foi alterado com sucesso!");

        }

        [Test]
        public void CriarProjeto()
        {

            #region Page, Flows Objects

            projectsFlows = new ProjectsFlows();
            projectsPage = new ProjectsPage();
            #endregion

            #region Parametros
            nomeProjeto = "Treinamento WEB Nayara Oliveira Teste Mobile 6";
            identificador = "NC0900";
            tipo = "Web";
            descricao = "o projeto by me";
            #endregion

            AcessarProjetosFlow();
            projectsFlows.CriarNovoProjeto(identificador, nomeProjeto, descricao, tipo);

            mensagemSucesso = projectsPage.RetornarMensagemDeSucesso();
            Assert.AreEqual(mensagemSucesso, "O Projeto ( " + nomeProjeto + " ) foi cadastrado com sucesso!");

        }

        [Test]
        public void ErroAoCriarProjeto()
        {

            #region Page, Flows Objects
            projectsFlows = new ProjectsFlows();
            projectsPage = new ProjectsPage();
            #endregion

            #region Parametros
            bool mensagemErroIdentificador = false;
            nomeProjeto = "Treinamento WEB Nayara Oliveira";
            identificador = "NC088";
            tipo = "Web";
            descricao = "o projeto by me";
            #endregion

            AcessarProjetosFlow();
            projectsFlows.CriarNovoProjeto(identificador, nomeProjeto, descricao, tipo);

            mensagemErroIdentificador = projectsPage.RetornarMensagemDeErroIdentificadorIgual();
            Assert.IsTrue(mensagemErroIdentificador);
        }

        [Test]
        public void VerificarCasoDeTeste()
        {
            #region Page, Flows Objects
            projectsPage = new ProjectsPage();
            projectPage = new ProjectPage();
            #endregion

            AcessarProjetosFlow();
            projectsPage.ClicarEmPrimeiroProjeto();
            projectPage.ClicarEmCasosDeTeste();

            primeiroCasoDeTeste = projectPage.RetornarTextoDoIdentificador();
            Assert.AreEqual("CT01", primeiroCasoDeTeste);
        }

        [Test]
        //todo refatorar para pegar o nome do projeto no assert
        public void ExcluirProjeto()
        {
            #region 
            projectsPage = new ProjectsPage();
            #endregion
            
            AcessarProjetosFlow();
            
            projectsPage.ExcluirProjetoById();
            Assert.IsNotNull(projectsPage.GetSuccessMessage());

        }

        [Test]
        public void AcessarBugTracker()
        {
            projectPage = new ProjectPage();


            AcessarProjetosFlow();
            projectsPage.ClicarEmPrimeiroProjeto();
            projectPage.ClicarEmBugTracker();
        }

        [Test]
        public void ExportarOcorrencias()
        {
            projectPage = new ProjectPage();

            AcessarProjetosFlow();
            projectsPage.ClicarEmPrimeiroProjeto();
            projectPage.ClicarEmBugTracker();
            projectPage.ClicarEmExportar();
        }

        [Test]
        public void CriarReleaseDeProjeto()
        {
            nomeRelease = "aaaaaaaaaaa";
            identificador = "R04";
            instrucoes = "Realizar testes utilizando navegador";
            valorMinimo = "2,00";
            vagas = "20";
            projectPage = new ProjectPage();
            projectFlows = new ProjectFlows();

            AcessarProjetosFlow();
            projectsPage.ClicarEmPrimeiroProjeto();
            projectFlows.CriarRelease(identificador, nomeRelease, instrucoes, valorMinimo, vagas);
            mensagemSucesso = projectPage.RetornarMensagemDeSucesso();
            Assert.AreEqual(mensagemSucesso, "Release " + nomeRelease + " cadastrada com sucesso!");

        }

        //todo Finalizar método de escolha de data no calendario
        [Test]
        public void VerificarEscolhaDeDataNoCalendario()
        {
            AcessarProjetosFlow();
            dashboardFlows = new DashboardFlows();
            atuarComoTestadorFlow = new AtuarComoTestadorFlow();
            dashboardFlows = new DashboardFlows();
            dashboardFlows.AcessarPaginaTestador();
            atuarComoTestadorFlow.testador();
        }
    }
}
