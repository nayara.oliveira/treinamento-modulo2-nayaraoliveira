﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class LoginTests : TestBase
    {
        LoginPage loginPage;
        LoginFlows loginFlows;
        
        public static IEnumerable loginUsingCSVData() {
            return GeneralHelpers.ReturnCSVData(GeneralHelpers.GetProjectPath() + "Resources/TestData/Login/login.csv");
        }

        public static IEnumerable cadastroDeUsuarioUsingCSVData()
        {
            return GeneralHelpers.ReturnCSVData(GeneralHelpers.GetProjectPath() + "Resources/TestData/Cadastro/cadastro.csv");
        }


        [Test]
        public void EfetuarLoginComSucesso()
        {
            #region Page, Flows Objects
            loginPage = new LoginPage();
            #endregion

            #region Parametros
            string usuario = BuilderJson.ReturnParameterAppSettings("USER");
            string senha = BuilderJson.ReturnParameterAppSettings("PASSWORD");
            string mensagemConfirmacaoLogin = "Bem-vindo! nayara.oliveira";
            string mensagem= "";
            #endregion

            loginPage.PreencherUsuario(usuario);
            loginPage.PreencherSenha(senha);
            loginPage.ClicarEmLogin();

            mensagem = loginPage.RetornarMensagemDeSucesso();
            Assert.AreEqual(mensagemConfirmacaoLogin, mensagem);
        }

        [Test, TestCaseSource("loginUsingCSVData")]
        public void InformarEmailInvalidoParaLogin(ArrayList testData)
        {
            #region Page, Flows Objects
            loginFlows = new LoginFlows();
            loginPage = new LoginPage();
            #endregion

            #region Parametros
            string usuario = testData[0].ToString();
            string senha = testData[1].ToString();
            string mensagemErroEsperada = "×\r\nUsuário ou senha inválidos!";
            string loginErrado = "";
            #endregion

            loginFlows.EfetuarLogin(usuario,senha);
            loginErrado = loginPage.RetornarMensagemDeErro();

            Assert.AreEqual(mensagemErroEsperada,loginErrado);

        }

        [Test]
        public void InformarEmailParaRedefinirSenha()
        {
            #region Page, Flows Objects
            loginFlows = new LoginFlows();
            loginPage = new LoginPage();
            #endregion

            #region Parametros
            string mensagemErroEsperada = "Redefinição de senha enviada com sucesso!";
            #endregion

            loginFlows.RedefinirSenha(BuilderJson.ReturnParameterAppSettings("USER"));
            Assert.AreEqual(mensagemErroEsperada, loginPage.RetornarMensagemDeErro() );
        }

        [Test, TestCaseSource("cadastroDeUsuarioUsingCSVData")]
        public void EfetuarCadastroDeUsuario(ArrayList testData)
        {
            #region Page, Flows Objects
            loginFlows = new LoginFlows();
            loginPage = new LoginPage();
            #endregion

            #region Parametros
            string login = testData[0].ToString();
            string email = testData[1].ToString();
            string nome = testData[2].ToString();
            string sobrenome = testData[3].ToString();
            string senha = testData[4].ToString();
            string mensagemEsperada = "×\r\nO Usuário ( "+nome+" ) foi cadastrado com sucesso!";
            #endregion

            loginFlows.CadastrarUsuario(login,email,nome,sobrenome,senha);
            Assert.AreEqual(mensagemEsperada,loginPage.RetornarMensagemDeErro());

        }
        
        [Test]
        public void RealizarLogout()
        {

            #region Parametros
            string usuario = BuilderJson.ReturnParameterAppSettings("USER");
            string senha = BuilderJson.ReturnParameterAppSettings("PASSWORD");
            #endregion

            string mensagemEsperada = "×\r\nLogout com sucesso!";
            loginFlows = new LoginFlows();
            loginPage = new LoginPage();

            loginFlows.EfetuarLogin(usuario, senha);
            loginFlows.EfetuarLogout();
            Assert.AreEqual(mensagemEsperada, loginPage.RetornarMensagemDeLogout());


        }

        [Test, TestCaseSource("cadastroDeUsuarioUsingCSVData")]
        public void TentarCadastrarSemInformarEmail(ArrayList testData)
        {
            #region Page, Flows Objects
            loginFlows = new LoginFlows();
            loginPage = new LoginPage();
            #endregion

            #region Parametros
            string login = "aaa";
            string email = "";
            string nome = testData[2].ToString();
            string sobrenome = testData[3].ToString();
            string senha = testData[4].ToString();
            string mensagemEsperada = "O e-mail deve ser informado!";
            #endregion

            loginFlows.CadastrarUsuario(login, email, nome, sobrenome, senha);
            Assert.AreEqual(mensagemEsperada, loginPage.RetornarMensagemEmailObrigatorio());

        }
        
        [Test, TestCaseSource("cadastroDeUsuarioUsingCSVData")]
        public void TentarCadastrarSemInformarLogin(ArrayList testData)
        {
            #region Page, Flows Objects
            loginFlows = new LoginFlows();
            loginPage = new LoginPage();
            #endregion

            #region Parametros
            string login = "";
            string email = "ritodi9269@webbamail.com";
            string nome = testData[2].ToString();
            string sobrenome = testData[3].ToString();
            string senha = testData[4].ToString();
            string mensagemEsperada = "O Login deve ser informado!";
            #endregion

            loginFlows.CadastrarUsuario(login, email, nome, sobrenome, senha);
            Assert.AreEqual(mensagemEsperada, loginPage.RetornarMensagemLoginObrigatorio());

        }
        
        [Test, TestCaseSource("cadastroDeUsuarioUsingCSVData")]
        public void TentarCadastrarSemInformarNome(ArrayList testData)
        {
            #region Page, Flows Objects
            loginFlows = new LoginFlows();
            loginPage = new LoginPage();
            #endregion

            #region Parametros
            string login = "aa";
            string email = "ritodi9269@webbamail.com";
            string nome = "";
            string sobrenome = testData[3].ToString();
            string senha = testData[4].ToString();
            string mensagemEsperada = "O nome deve ser informado!";
            #endregion

            loginFlows.CadastrarUsuario(login, email, nome, sobrenome, senha);
            Assert.AreEqual(mensagemEsperada, loginPage.RetornarMensagemNomeObrigatorio());

        }
        
        [Test, TestCaseSource("cadastroDeUsuarioUsingCSVData")]
        public void TentarCadastrarSemInformarSobrenome(ArrayList testData)
        {
            #region Page, Flows Objects
            loginFlows = new LoginFlows();
            loginPage = new LoginPage();
            #endregion

            #region Parametros
            string login = "aa";
            string email = "ritodi9269@webbamail.com";
            string nome = "aaaa";
            string sobrenome = "";
            string senha = testData[4].ToString();
            string mensagemEsperada = "O sobrenome deve ser informado!";
            #endregion

            loginFlows.CadastrarUsuario(login, email, nome, sobrenome, senha);
            Assert.AreEqual(mensagemEsperada, loginPage.RetornarMensagemSobrenomeObrigatorio());

        }

        //todo verificar qual o erro de senha menor que 4
        [Test, TestCaseSource("cadastroDeUsuarioUsingCSVData")]
        public void ValidarTipoDeSenha(ArrayList testData)
        {
            #region Page, Flows Objects
            loginFlows = new LoginFlows();
            loginPage = new LoginPage();
            #endregion

            #region Parametros
            string login = testData[0].ToString();
            string email = testData[1].ToString();
            string nome = testData[2].ToString();
            string sobrenome = testData[3].ToString();
            string senha = testData[4].ToString();
            string mensagemEsperada = "A senha deve conter no mínimo 4 caracteres. Não é permitido somente espaços!";
            #endregion

            loginFlows.CadastrarUsuario(login, email, nome, sobrenome, senha);
            Assert.AreEqual(mensagemEsperada, loginPage.RetornarMensagemTipoSenha());

        }
    }
}