﻿using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Flows
{
    public class ProjectsFlows
    {
        ProjectsPage projectsPage;
        
        #region Construtor
        public ProjectsFlows()
        {
            projectsPage = new ProjectsPage();
        }
        #endregion

        #region Flows
        public void AcessarPrimeiroProjeto()
        {
            projectsPage.ClicarEmPrimeiroProjeto();
        }

        public void EditarDadosDeProjeto(string nomeProjeto)
        {
            projectsPage.ClicarEmPrimeiroProjeto();
            projectsPage.ClicarEmEditarProjeto();
            projectsPage.PreencherNomeProjeto(nomeProjeto);
            projectsPage.SalvarAlteracao();
        }
        public void CriarNovoProjeto(string identificador, string nomeProjeto, string descricao, string tipo)
        {
            projectsPage.ClicarEmIncluir();
            projectsPage.PreencherIdentificador(identificador);
            projectsPage.PreencherNomeProjeto(nomeProjeto);
            projectsPage.PreencherDescricao(descricao);
            projectsPage.SelecionarTipoDeProjeto(tipo);
            projectsPage.SalvarAlteracao();
        }

        public void ExcluirProjeto()
        {
            projectsPage.ExcluirProjetoById();
        }

        #endregion
    }
}
