﻿using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Flows
{
    public class ProjectFlows
    {

        ProjectPage projectPage;

        public ProjectFlows()
        {
            projectPage = new ProjectPage();
        }

        public void CriarRelease(string identificador, string nome, string instrucoes, string valorMinimo,string vagas)
        {
            projectPage.ClicarEmRelease();
            projectPage.ClicarEmIncluirRelease();
            projectPage.PreencherIdentificadorRelease(identificador);
            projectPage.PreencherNomeReleaseField(nome);
            projectPage.PreencherInstrucoes(instrucoes);
            projectPage.PreencherValorMinimo(valorMinimo);
            projectPage.PreencherVagas(vagas);
            projectPage.SelecionarTestCase();
            projectPage.ClicarEmSalvarRelease();
            projectPage.ClicarEmConfirmarRelease();
        }

    }
}
