﻿using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Flows
{
    public class TestCaseFlows
    {

        TestCasePage testCasePage;

        #region Page Objects and Flows
        public TestCaseFlows()
        {
            testCasePage = new TestCasePage();
        }
        #endregion

        #region
        public void VerificarCasoDeTeste()
        {
            testCasePage.ClicarEmCasosDeTeste();
        }
        #endregion
    }
}
