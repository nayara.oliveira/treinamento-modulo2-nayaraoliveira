﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    class ProjectPage : PageBase
    {
        #region Mapping Bug Tracker
        private By bugTrackerTab = By.Id("li-bug-tracker-link");
        private By casoDeTesteTab = By.XPath("//*[@id='li-test-cases-link']");
        private By identificadorBy = By.XPath("//td[2][1]");
        private By exportarButton = By.XPath("//*[@id='project_bug_tracker_container']/div[1]/div[2]/div/div/a");
        
        private By releaseTab = By.Id("li-releases-link");
        private By identificadorReleaseField = By.Id("release_identifier_id");
        private By nomeReleaseField = By.Id("release_name");
        private By instrucoesTextArea = By.XPath("//form[@id='releaseForm']/fieldset/div[3]/div/div[3]/div[3]");
        private By valorMinimoField = By.Id("release_release_value");
        private By numeroDeVagasField = By.Id("tester_number_field");
        private By salvarReleaseButton = By.XPath("//button[@class='ppt-btn-default ppt-btn-save']");
        private By testCase = By.Id("chk_release_release_test_cases_id_NIL");
        private By confirmarReleaseButton = By.Id("modal_confirm_button");
        private By sucessoAoCriarRelease = By.XPath("//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-success']");
        #endregion

        #region Actions
        public void ClicarEmCasosDeTeste()
        {
            WaitForElement(casoDeTesteTab);
            Click(casoDeTesteTab);
        }

        public string RetornarTextoDoIdentificador()
        {
            return driver.FindElement(identificadorBy).Text;
        }

        #region BugTracker
        public void ClicarEmBugTracker()
        {
            Click(bugTrackerTab);
        }
        
        public void ClicarEmExportar()
        {
            WaitForElement(exportarButton);
            Click(exportarButton);
        }
        #endregion

        #region Release
        public void ClicarEmRelease()
        {
            Click(releaseTab);
        }
        
        public void ClicarEmIncluirRelease()
        {
            driver.FindElement(By.XPath("(//a[contains(text(),'Incluir')])[2]")).Click();
        }

        public void PreencherIdentificadorRelease(string identificador)
        {
            SendKeys(identificadorReleaseField,identificador);
        }
       
        public void PreencherNomeReleaseField(string nome)
        {
            SendKeys(nomeReleaseField, nome);

        }
        
        public void PreencherInstrucoes(string instrucoes)
        {
            SendKeys(instrucoesTextArea, instrucoes);
        }

        public void PreencherValorMinimo(string valorMinimo)
        {
            SendKeys(valorMinimoField, valorMinimo);
        }

        public void PreencherVagas(string vagas)
        {
            SendKeys(numeroDeVagasField, vagas);
        }

        public void SelecionarTestCase()
        {
            Click(testCase);
        }

        public void ClicarEmSalvarRelease()
        {
            Click(salvarReleaseButton);
        }

        public void ClicarEmConfirmarRelease()
        {
            Click(confirmarReleaseButton);
        }

        public string RetornarMensagemDeSucesso()
        {
            return GetText(sucessoAoCriarRelease);
        }

        #endregion
        #endregion

    }
}
