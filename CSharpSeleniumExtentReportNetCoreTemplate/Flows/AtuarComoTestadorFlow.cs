﻿using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Flows
{
    class AtuarComoTestadorFlow
    {

        AtuarComoTestadorPage atuarComoTestadorPage;

        #region Page Object and Constructor
        public AtuarComoTestadorFlow()
        {
            atuarComoTestadorPage = new AtuarComoTestadorPage();
        }
        public void testador()
        {
            atuarComoTestadorPage = new AtuarComoTestadorPage();
            atuarComoTestadorPage.ClicarEmExtrato();
        }
        #endregion
    }
}
