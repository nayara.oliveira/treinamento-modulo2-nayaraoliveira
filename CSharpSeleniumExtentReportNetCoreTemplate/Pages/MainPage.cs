﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class MainPage : PageBase
    {
        #region Mapping
        private By gerenciarProjetosBy = By.XPath("//a[@href='/backoffice/dashboard']");
        #endregion


        #region Actions
        public void ClicarEmGerenciarProjetos()
        {
            Click(gerenciarProjetosBy);
        }
        #endregion
    }
}
